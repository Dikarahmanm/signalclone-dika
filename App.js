import "react-native-gesture-handler"
import React from 'react'
import { NavigationContainer } from '@react-navigation/native';
import { StatusBar } from 'expo-status-bar';
import { StyleSheet, Text, View, AppRegistry } from 'react-native';
import { createStackNavigator } from "@react-navigation/stack"
import LoginScreen from "./screens/LoginScreen";
import RegisterScreen from "./screens/RegisterScreen";
import HomeScreen from "./screens/HomeScreen"
import AddChat from "./screens/AddChatScreen"
import ChatScreen from "./screens/ChatScreen";



const Stack = createStackNavigator();


const globalScreenOptions={
  headerStyle:{ backgroundColor:'#2C6BED'},
  headerTitleStyle:{ color:"white"},
  headerTintColor:"white",
};

export default function App() {
  return (
    <NavigationContainer>
      <Stack.Navigator
        //initialRouteName="Home"
        screenOptions={globalScreenOptions}>
        <Stack.Screen name="Login" component={LoginScreen} />
        <Stack.Screen name="Register" component={RegisterScreen} />
        <Stack.Screen name="Home" component={HomeScreen} />
        <Stack.Screen name="AddChat" component={AddChat} />
        <Stack.Screen name="Chat" component={ChatScreen} />
      </Stack.Navigator>
    </NavigationContainer>
    
  );
}
AppRegistry.registerComponent('Appname', () => App);
const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
