import { StyleSheet, Text, View } from 'react-native'
import React from 'react'
import { Avatar, ListItem } from 'react-native-elements'

const CustomListItem = ({id, chatName, enterChat}) => {
    return (
        <ListItem onPress={()=> enterChat(id,chatName)} key={id} bottomDivider >
            <Avatar
                rounded
                source={{
                    uri: 
                    "https://i.pinimg.com/736x/b7/32/e3/b732e3f2d7f9e33ad0908db1881ac624--bar-scene-trevi-fountain.jpg",

                }}
            />
            <ListItem.Content>
                <ListItem.Title style={{ fontWeight:"800"}}>
                {chatName}
                </ListItem.Title>
                <ListItem.Subtitle numberOfLines={1} ellipsizeMode="tail">
                ABC
                </ListItem.Subtitle>
            </ListItem.Content>
        </ListItem>
    )
}

export default CustomListItem

const styles = StyleSheet.create({})