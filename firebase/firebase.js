import firebase from 'firebase/compat/app';
import 'firebase/compat/auth';
import 'firebase/compat/firestore';


const firebaseConfig = {
  apiKey: "AIzaSyB8mFnnGvz4p9oI50VrIj1NufyOv72l4W0",
  authDomain: "signalclone-dika.firebaseapp.com",
  projectId: "signalclone-dika",
  storageBucket: "signalclone-dika.appspot.com",
  messagingSenderId: "923381960998",
  appId: "1:923381960998:web:09b7379b504f61138871e7"
};

let app;

if (firebase.apps.length === 0) {
  app = firebase.initializeApp(firebaseConfig)
} else {
  app = firebase.app();
}

const db = app.firestore();
const auth = firebase.auth();

export { db, auth };