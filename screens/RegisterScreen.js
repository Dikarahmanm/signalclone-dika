import { KeyboardAvoidingView, StyleSheet, View } from 'react-native'
import React, { useLayoutEffect, useState } from 'react'
import { StatusBar } from 'expo-status-bar'
import { Button, Input, Text } from 'react-native-elements'
import { auth } from '../firebase/firebase'

const RegisterScreen = ({ navigation }) => {
    const [name, setName] = useState("")
    const [email, setEmail] = useState("")
    const [password, setPassword] = useState("")
    const [imageUrl, setImageUrl] = useState("")
    
    useLayoutEffect(() => {
        navigation.setOptions({
            headerBackTitle:'Back to Login',
        })
    }, [navigation])

    const register=() =>{
        auth
        .createUserWithEmailAndPassword(email,password)
        .then((authUser)=>{
            authUser.user.updateProfile({
                displayName: name,
                photoURL: imageUrl || "https://i.pinimg.com/736x/b7/32/e3/b732e3f2d7f9e33ad0908db1881ac624--bar-scene-trevi-fountain.jpg"
            })
        })
        .catch((error) =>alert(error.message))
    }
    
        return (
            <KeyboardAvoidingView behaviour ="padding" style={styles.container}>
                <StatusBar style='light' />
                <Text h3 style={{ marginBottom: 50, alignItems:'center'}}>
                    Create a Signal account
                </Text>

                <View style={styles.inputContainer}>
                    <Input 
                    placeholder="Full Name" 
                    autoFocus type="text" 
                    value={name} 
                    onChangeText={text => setName(text)}
                />
                <Input 
                    placeholder="Email" 
                    autoFocus type="text" 
                    value={email} 
                    onChangeText={text => setEmail(text)}
                />
                <Input 
                    placeholder="Password" 
                    autoFocus type="text" 
                    value={password} 
                    onChangeText={text => setPassword(text)}
                />
                <Input 
                    placeholder="Profile Picture URL (Optional) " 
                    autoFocus type="text" 
                    value={imageUrl} 
                    onChangeText={text => setImageUrl(text)}
                    onSubmitEditing={register}
                    
                />
                
                    
                </View>
                <Button
                    containerStyle={styles.button}
                    raised
                    onPress={register}
                    title='Register'
                />
                <View style={{height:100 }} />
            </KeyboardAvoidingView>
        )
}

export default RegisterScreen

const styles = StyleSheet.create({
    container: {
    flex: 1,
    backgroundColor: 'white',
    alignItems: 'center',
    justifyContent: 'center',
    },
    button:{
        width:200,
        marginTop:10,
    },
    inputContainer:{
        width:300,
    },
})