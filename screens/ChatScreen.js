import { KeyboardAvoidingView, Platform, SafeAreaView, ScrollView, StatusBar, StyleSheet, Text, TextInput, Touchable, View } from 'react-native'
import React, { useLayoutEffect } from 'react'
import { Avatar } from 'react-native-elements'
import { TouchableOpacity } from 'react-native-gesture-handler'
import { AntDesign, FontAwesome, Ionicons } from '@expo/vector-icons'

const ChatScreen = ({navigation, route}) => {

    useLayoutEffect(() => {
        navigation.setOptions({
            title: "Chat",
            //headerBackTitleVisibele: false,
            headerTitleAlign:"left",
            headerTitle: () => (
                <View
                    style={{
                        flexDirection:"row",
                        alignItems: "center",
                    }}
                    >
                        <Avatar
                        rounded
                        source={{
                            uri:
                            "https://i.pinimg.com/736x/b7/32/e3/b732e3f2d7f9e33ad0908db1881ac624--bar-scene-trevi-fountain.jpg"
                        }}
                        />
                        <Text style={{color:"white", marginLeft:10, fontWeight:700,}}
                        >
                        {route.params.chatName}
                        </Text>
                    </View>
                ),
                headerLeft: () =>(
                    <TouchableOpacity>
                        <AntDesign name="arrowleft" size={24} color='white'/>
                    </TouchableOpacity>
                ),
                headerRight: () => (
                    <View
                    style={{
                            flexDirection:"row",
                            justifyContent: "space-between",
                            width:80,
                            marginRight:20,
                        }}
                        >
                        <TouchableOpacity>
                            <FontAwesome name="video-camera" size={24} color="white"/>
                        </TouchableOpacity>
                        <TouchableOpacity>
                            <Ionicons name="call" size={24} color="white"/>
                        </TouchableOpacity>
                    </View>
                )  
            })
    },[navigation])

    return (
        <SafeAreaView style={{ flex: 1, backgroundColor:'white'}}>
        <StatusBar style="light" />
        <KeyboardAvoidingView
        behavior={Platform.OS === "ios" ? "padding" : "height"}
        style={styles.container}
        keyboardVerticalOffset={90}
        >
            <>
                <ScrollView>
                    {/*Chat goes here*/}
                </ScrollView>
                <View style={styles.footer}>
                    <TextInput placeholder='Signal Message' />
                </View>
            </>

        </KeyboardAvoidingView>
        </SafeAreaView>
    )
}

export default ChatScreen

const styles = StyleSheet.create({
    container:{},
    footer:{},
})